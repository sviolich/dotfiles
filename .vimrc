

"NeoBundle Scripts-----------------------------
if has('vim_starting')
  if &compatible
    set nocompatible               " Be iMproved
  endif

  " Required:
  set runtimepath+=/home/steve/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('/home/steve/.vim/bundle'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" Add or remove your Bundles here:
NeoBundle 'airblade/vim-gitgutter'
NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'ctrlpvim/ctrlp.vim'
NeoBundle 'weynhamz/vim-plugin-minibufexpl'
NeoBundle 'markabe/bufexplorer'
NeoBundle 'majutsushi/tagbar'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'scrooloose/syntastic'
NeoBundle 'taglist.vim'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'tpope/vim-unimpaired'
NeoBundle 'Valloric/YouCompleteMe'

" You can specify revision/branch/tag.
NeoBundle 'Shougo/vimshell', { 'rev' : '3787e5' }

" Required:
call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck
"End NeoBundle Scripts-------------------------

" Add the g flag to search/replace by default
set gdefault
" Use UTF-8 without BOM
set encoding=utf-8 nobomb
" Change mapleader
let mapleader=","

" Don’t add empty newlines at the end of files
set binary
set noeol
" Centralize backups, swapfiles and undo history
set backupdir=~/.vim/backups
set directory=~/.vim/swaps
if exists("&undodir")
	set undodir=~/.vim/undo
endif


" Show line numbers
set number
" Enable syntax highlighting
syntax on
" Highlight current line
set cursorline
" Highlight 80th column
set colorcolumn=80,120
" Make tabs four spaces wide
set tabstop=4
set shiftwidth=4
set expandtab
" Show “invisible” characters
set list
set listchars=tab:▶\ ,trail:·,eol:¬,nbsp:_
" Highlight searches
set hlsearch
" Ignore case of searches
set ignorecase
" Highlight dynamically as pattern is typed
set incsearch
" Always show status line
set laststatus=2
" Enable mouse in all modes
set mouse=a
" Disable error bells
set noerrorbells
" Don’t reset cursor to start of line when moving around.
set nostartofline
" Don’t show the intro message when starting Vim
set shortmess=atI
" Hide the current mode (Powerline will show it)
" (powerline currently not working)
set showmode
" Show the filename in the window titlebar
set title
" Show the (partial) command as it’s being typed
set showcmd
" Start scrolling three lines before the horizontal window border
set scrolloff=3

" Configure Solarized color scheme.
set t_Co=16

set background=dark  " leave as dark, see .Xresources for flipping light/dark.
colorscheme solarized

" Set status line colors (AFTER setting color scheme).
"hi StatusLine ctermbg=10 ctermfg=0
hi StatusLine ctermbg=0 ctermfg=4
hi StatusLineNC ctermbg=0 ctermfg=10
hi VertSplit ctermbg=10 ctermfg=10

" Currently does green for insert, red for replace.
" TODO colors for different types of selection (normal, line and block)
function! InsertStatuslineColor(mode)
  if a:mode == 'i'
    hi statusline ctermfg=2
  elseif a:mode == 'r'
    hi statusline ctermfg=1
  else
    hi statusline ctermfg=3
  endif
endfunction

au InsertEnter * call InsertStatuslineColor(v:insertmode)
au InsertChange * call InsertStatuslineColor(v:insertmode)
au InsertLeave * hi StatusLine ctermfg=4

" Set MiniBufExplorer colors.
hi MBENormal ctermbg=0 ctermfg=10
hi MBEChanged ctermbg=0 ctermfg=10
hi MBEVisibleNormal ctermbg=8 ctermfg=14
hi MBEVisibleChanged ctermbg=8 ctermfg=14
hi MBEVisibleActiveNormal ctermbg=4 ctermfg=4
hi MBEVisibleActiveChanged ctermbg=4 ctermfg=4

" Configure Syntastic.
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
let g:syntastic_aggregate_errors = 1
let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_checkers = ['pep8']
let g:syntastic_python_pep8_post_args="--max-line-length=120"

" Configure Jedi.
" Don't pop up docstring window (at top).
"autocmd FileType python setlocal completeopt=menuone,longest
"let g:jedi#popup_select_first = 1
"let g:jedi#use_tabs_not_buffers = 0

" Configure ctrlp.
" Ignore *.pyc files.
let g:ctrlp_custom_ignore = '.pyc$'

" Configure NERDTree.
" Ignore *.pyc files.
let NERDTreeIgnore=['\.pyc$']

" = KEY MAPPINGS =

" Ctrl-hjkl switches windows
map <C-k> <C-w><Up>
map <C-j> <C-w><Down>
map <C-l> <C-w><Right>
map <C-h> <C-w><Left>

" F1 = Esc. No more accidentally opening help.
:map <F1> <Esc>

" Leader c/C (comment) comments/decomments selection.
:noremap <Leader>c :s/^/#<CR>:noh<CR>
:noremap <Leader>C :s/^#/<CR>:noh<CR>

" - Insert mode -

" F1 = Esc. No more accidentally opening help.
:imap <F1> <Esc>

" jj = Esc. Easier to reach.
:inoremap jj <Esc>

" - Normal mode -

" F2 toggles nerdtree for filesystem.
:nnoremap <F2> :NERDTreeToggle<CR><C-w>=

" F3 opens bufexplorer for buffers.
:nnoremap <F3> :BufExplorer<CR>

" F4 toggles tagbar for code structure.
:nnoremap <F4> :TagbarToggle<CR><C-w>=

" Leader b/B (breakpoint) inserts a breakpoint below/above line.
:nnoremap <Leader>b oimport ipdb; ipdb.set_trace()<Esc>
:nnoremap <Leader>B Oimport ipdb; ipdb.set_trace()<Esc>

" Leader pp/PP (pretty print) inserts a pretty print below/above line.
:nnoremap <Leader>pp ofrom pprint import pprint; pprint()<Esc>i
:nnoremap <Leader>PP Ofrom pprint import pprint; pprint()<Esc>i

" Leader pb/pt (p-buffer/p-tag) open fuzzy finder for buffers/tags.
:nnoremap <Leader>pb :CtrlPBuffer<CR>
:nnoremap <Leader>pt :CtrlPTag<CR>

" Leader gb (git blame) shows git blame window.
:nnoremap <Leader>gb :Gblame<CR>

" Leader h (highlight) removes current highlighting.
:nnoremap <Leader>h :noh<CR>

" Leader tn/tc (tab new/tab close) create new or close current tabs.
:nnoremap <Leader>tn :tabnew<CR>
:nnoremap <Leader>tc :tabclose<CR>

" Leader w (ctrl-w on windows) deletes current buffer and switches to previous, maintain window.
:nnoremap <Leader>w :b#\|bd#<CR>

" Semi-colon = colon. No need to press Shift.
:nmap ; :