#!/bin/bash

# Update package lists and existing packages.
apt-get update
apt-get upgrade --yes

#==============================================================================
# Development
#==============================================================================

# Install packages.
apt-get install --yes git
apt-get install --yes python
apt-get install --yes python-pip
apt-get install --yes vim
apt-get install --yes virtualenvwrapper

# Prepare centralised Vim dirs (see .vimrc).
mkdir --parents ~/.vim/backups ~/.vim/swaps ~/.vim/undo
chown $SUDO_USER:$SUDO_USER ~/.vim/backups ~/.vim/swaps ~/.vim/undo

# Install NeoBundle plugin manager for Vim. 
curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh | bash

#==============================================================================
# User Interface (herbstluftwm)
#==============================================================================

# Install packages.
apt-get install --yes dmenu
apt-get install --yes feh
apt-get install --yes herbstluftwm

#==============================================================================
# Other Software
#==============================================================================

# Enable MultiArch for i386 packages (required for Skype).
dpkg --add-architecture i386

# Add key for Google repository (required for Chrome).
if [[ ! `apt-key list` =~ 'Google, Inc. Linux Package Signing Key' ]]; then
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
fi

# Add repositories.
sources_file=/etc/apt/sources.list
google_repo="deb http://dl.google.com/linux/chrome/deb/ stable main"
if [[ ! `grep "^$google_repo" $sources_file` ]]; then
    add-apt-repository "$google_repo"
fi
canonical_repo="deb http://archive.canonical.com/ $(lsb_release -sc) partner"
if [[ ! `grep "^$canonical_repo" $sources_file` ]]; then
    add-apt-repository "$canonical_repo"
fi

# Update package lists.
apt-get update

# Install packages.
apt-get install --yes google-chrome-stable
apt-get install --yes skype

#==============================================================================
# Laptop
#==============================================================================

# If on laptop, install power/backlight packages.
if [[ `upower -e` =~ 'line_power_AC0' ]]; then
    apt-get install --yes acpi
    apt-get install --yes xbacklight
fi
		
#==============================================================================
# Dotfiles
#==============================================================================

# Sync from source dir to home dir and reload bash.
source_dir="$(dirname "${BASH_SOURCE}")"
rsync -ahv --exclude ".git" --exclude "install.sh" --exclude ".*.swp" $source_dir ~
source ~/.bashrc
