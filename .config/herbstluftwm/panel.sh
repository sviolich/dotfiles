#!/bin/bash

script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
icons_dir="$script_dir/icons"

hc() { "${herbstclient_command[@]:-herbstclient}" "$@" ;}
monitor=${1:-0}
geometry=( $(herbstclient monitor_rect "$monitor") )
if [ -z "$geometry" ] ;then
    echo "Invalid monitor $monitor"
    exit 1
fi
# geometry has the format W H X Y
x=${geometry[0]}
y=${geometry[1]}
panel_width=${geometry[2]}
panel_height=16
font="-*-fixed-medium-*-*-*-12-*-*-*-*-*-*-*"

# colors
base03='#002b36'
base02='#073642'
base01='#586e75'
base00='#657b83'
base0='#839496'
base1='#93a1a1'
base2='#eee8d5'
base3='#fdf6e3'
yellow='#b58900'
orange='#cb4b16'
red='#dc322f'
magenta='#d33682'
violet='#6c71c4'
blue='#268bd2'
cyan='#2aa198'
green='#859900'

bgcolor=$(hc get frame_border_normal_color)
selbg=$(hc get window_border_active_color)
selfg=$base03

####
# Try to find textwidth binary.
# In e.g. Ubuntu, this is named dzen2-textwidth.
if which textwidth &> /dev/null ; then
    textwidth="textwidth";
elif which dzen2-textwidth &> /dev/null ; then
    textwidth="dzen2-textwidth";
else
    echo "This script requires the textwidth tool of the dzen2 project."
    exit 1
fi
####
# true if we are using the svn version of dzen2
# depending on version/distribution, this seems to have version strings like
# "dzen-" or "dzen-x.x.x-svn"
if dzen2 -v 2>&1 | head -n 1 | grep -q '^dzen-\([^,]*-svn\|\),'; then
    dzen2_svn="true"
else
    dzen2_svn=""
fi

if awk -Wv 2>/dev/null | head -1 | grep -q '^mawk'; then
    # mawk needs "-W interactive" to line-buffer stdout correctly
    # http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=593504
    uniq_linebuffered() {
      awk -W interactive '$0 != l { print ; l=$0 ; fflush(); }' "$@"
    }
else
    # other awk versions (e.g. gawk) issue a warning with "-W interactive", so
    # we don't want to use it there.
    uniq_linebuffered() {
      awk '$0 != l { print ; l=$0 ; fflush(); }' "$@"
    }
fi

hc pad $monitor $panel_height

{
    ### Event generator ###
    # based on different input data (mpc, date, hlwm hooks, ...) this generates events, formed like this:
    #   <eventname>\t<data> [...]
    # e.g.
    #   date    ^fg(#efefef)18:33^fg(#909090), 2013-10-^fg(#efefef)29

    #mpc idleloop player &
    while true ; do
        # "date" output is checked once a second, but an event is only
        # generated if the output changed compared to the previous run.
        date +$'date\t^fg(#efefef)%H:%M^fg(#909090), %Y-%m-^fg(#efefef)%d'
        sleep 1 || break
    done > >(uniq_linebuffered) &
    childpid=$!
    hc --idle
    kill $childpid
} 2> /dev/null | {
    IFS=$'\t' read -ra tags <<< "$(hc tag_status $monitor)"
    visible=true
    date=""
    windowtitle=""
    while true ; do

        ### Output ###
        # This part prints dzen data based on the _previous_ data handling run,
        # and then waits for the next event to happen.

        bordercolor="#26221C"
        separator="^bg()^fg($selbg)|"
        # draw tags
        for i in "${tags[@]}" ; do
            case ${i:0:1} in
                '#')  # Active monitor, active tag.
                    echo -n "^bg($selbg)^fg($selfg)"
                    ;;
                '-')  # Active monitor, active tag on inactive monitor
                    echo -n "^bg($base1)^fg($selfg)"
                    ;;
                '+')  # Inactive monitor, active tag.
                    #echo -n "^bg(#9CA668)^fg(#141414)"
                    echo -n "^bg($orange)^fg($selfg)"
                    ;;
                '%')  # Inactive monitor, active tag on active monitor
                    echo -n "^bg($base01)^fg($selfg)"
                    ;;
                ':')  # Inactive tag.
                    echo -n "^bg()^fg($base3)"
                    ;;
                '!')  # Inactive tag with notification?
                    echo -n "^bg(#FF0675)^fg(#141414)"
                    ;;
                *)  # Default
                    echo -n "^bg()^fg(#ababab)"
                    ;;
                # - active monitor, active tag on inactive monitor
                # % inactive monitor, active tag on active monitor
            esac
            if [ ! -z "$dzen2_svn" ] ; then
                # clickable tags if using SVN dzen
                echo -n "^ca(1,\"${herbstclient_command[@]:-herbstclient}\" "
                echo -n "focus_monitor \"$monitor\" && "
                echo -n "\"${herbstclient_command[@]:-herbstclient}\" "
                #echo -n "use \"${i:1}\") ${i:1} ^ca()"
                case ${i:1} in
                    "1") name=Dev1 ;;
                    "2") name=Dev2 ;;
                    "3") name=Dep ;;
                    "6") name=Web ;;
                    "7") name=ToDo ;;
                    "8") name=Msg ;;
                    "9") name=Oth ;;
                    *) name=${i:1} ;;
                esac
                echo -n "use \"${i:1}\") $name ^ca()"
            else
                # non-clickable tags if using older dzen
                echo -n " ${i:1} "
            fi
        done
        echo -n "$separator"
        echo -n "^bg()^fg() ${windowtitle//^/^^}"
        # network
        connected_devices=`nmcli device status | grep ' connected'`
        icon=''
        if [[ $connected_devices =~ eth0|usb0 ]]; then
            #icon='net_wired.xbm'
            icon='eth'
        elif [[ $connected_devices =~ wlan0 ]]; then
            #quality_raw=`iwconfig 2>&1 | grep -oP '\d+/70'`
            #quality_percent=`echo $quality_raw*100 | bc --mathlib | cut -d. -f1`
            #icon='wifi_01.xbm'
            icon='wifi'
        fi
        #network="^fg(#909090)^i($icons_dir/$icon)"
        network="^fg(#909090)$icon"
        # if on laptop, show power and battery
        if [[ `upower -e` =~ 'line_power_AC0' ]]; then
            # battery
            level=`acpi | sed -e 's/[,%]//g' | cut -d' ' -f4`
            if [[ `acpi --ac-adapter` =~ 'on-line' ]]; then
                color='green'
            elif [[ $level -gt 20 ]]; then
                color='yellow'
            elif [[ $level -gt 10 ]]; then
                color='orange'
            else
                color='red'
            fi
            if [[ $level -gt 70 ]]; then
                icon='bat_full_01.xbm'
            elif [[ $level -gt 40 ]]; then
                icon='bat_high_01.xbm'
            elif [[ $level -gt 10 ]]; then
                icon='bat_low_01.xbm'
            else
                icon='bat_empty_01.xbm'
            fi
            battery="^fg($color)^i($icons_dir/$icon)"
        fi
        # volume
        level=`amixer get Master | grep 'Mono:' | cut -d' ' -f5`
        volume="^fg(#909090)vol^fg(white)$level"
        # small adjustments
        right="$battery $volume $network $separator $date"
        right_text_only=$(echo -n "$right" | sed 's.\^[^(]*([^)]*)..g')
        # get width of right aligned text.. and add some space..
        width=$($textwidth "$font" "$right_text_only     ")
        echo -n "^pa($(($panel_width - $width)))$right"
        echo

        ### Data handling ###
        # This part handles the events generated in the event loop, and sets
        # internal variables based on them. The event and its arguments are
        # read into the array cmd, then action is taken depending on the event
        # name.
        # "Special" events (quit_panel/togglehidepanel/reload) are also handled
        # here.

        # wait for next event
        IFS=$'\t' read -ra cmd || break
        # find out event origin
        case "${cmd[0]}" in
            tag*)
                #echo "resetting tags" >&2
                IFS=$'\t' read -ra tags <<< "$(hc tag_status $monitor)"
                ;;
            date)
                #echo "resetting date" >&2
                date="${cmd[@]:1}"
                ;;
            quit_panel)
                exit
                ;;
            togglehidepanel)
                currentmonidx=$(hc list_monitors | sed -n '/\[FOCUS\]$/s/:.*//p')
                if [ "${cmd[1]}" -ne "$monitor" ] ; then
                    continue
                fi
                if [ "${cmd[1]}" = "current" ] && [ "$currentmonidx" -ne "$monitor" ] ; then
                    continue
                fi
                echo "^togglehide()"
                if $visible ; then
                    visible=false
                    hc pad $monitor 0
                else
                    visible=true
                    hc pad $monitor $panel_height
                fi
                ;;
            reload)
                exit
                ;;
            focus_changed|window_title_changed)
                windowtitle="${cmd[@]:2}"
                ;;
            #player)
            #    ;;
        esac
    done

    ### dzen2 ###
    # After the data is gathered and processed, the output of the previous block
    # gets piped to dzen2.

} 2> /dev/null | dzen2 -w $panel_width -x $x -y $y -fn "$font" -h $panel_height \
    -e 'button3=' \
    -ta l -bg "$bgcolor" -fg '#efefef'
